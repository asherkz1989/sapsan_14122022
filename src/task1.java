import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class task1 {

    public static void main(String[] args) {

        var nums = new int[]{2, 2, -3, 4};
        var min = Collections.min(Arrays.stream(nums).boxed().collect(Collectors.toList()));
        System.out.println(min);
    }
}
